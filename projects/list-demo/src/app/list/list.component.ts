import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChange, TemplateRef} from '@angular/core';
import {ListColumnWidthDescriptor} from './listColumnWidthDescriptor';

@Component({
  selector: 'luaks-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent<T> implements OnChanges {

  constructor() {
  }

  @Input() data: T[];
  @Input() columns: ListColumnWidthDescriptor[];

  @Input() row: TemplateRef<any>;

  gridColumnTemplate = '';

  static calculateGridTemplate(columns: ListColumnWidthDescriptor[]): string {
    return columns.map(column => column.width).join(' ');
  }

  ngOnChanges(changes: { data?: SimpleChange, columns?: SimpleChange }): void {
    if (changes.columns && this.columns) {
      this.gridColumnTemplate = ListComponent.calculateGridTemplate(this.columns);
    }
  }

  trackByIndex(index: number): number {
    return index;
  }
}
