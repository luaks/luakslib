import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  QueryList,
  SimpleChange,
  ViewChildren
} from '@angular/core';
import {ListColumnDescriptor} from '../list-header/list-column.descriptor';
import {fromEvent} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

function isEqualTo<T>(left: T): (right: T) => boolean {
  return (right: T) => left === right;
}

@Component({
  selector: 'luaks-list-resizeable-header',
  templateUrl: './list-resizeable-header.component.html',
  styleUrls: [
    '../list-header/list-header.component.scss',
    './list-resizeable-header.component.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListResizeableHeaderComponent implements OnChanges {
  @Input() columns: ListColumnDescriptor[];

  @ViewChildren('.list-header__column', {read: ElementRef}) columnElements: QueryList<ElementRef>;

  @Output()
  private columnWidthsUpdated: EventEmitter<ListColumnDescriptor[]> = new EventEmitter<ListColumnDescriptor[]>();

  columnDescriptors: ListColumnDescriptor[] = [];

  ngOnChanges(changes: { columns?: SimpleChange }): void {
    if (changes.columns) {
      this.columnDescriptors = this.columns;
    }
  }

  performResizeFront($event: MouseEvent,
                     column: ListColumnDescriptor,
                     firstColumn: boolean,
                     columnElement: HTMLElement) {
    if (!firstColumn) {
      fromEvent(window, 'mousemove')
        .pipe(takeUntil(fromEvent(window, 'mouseup')))
        .subscribe((mouseMove: MouseEvent) => {
          this.columnDescriptors =
            this.recalculateColumnDescriptors(
              mouseMove.movementX,
              Array.from(columnElement.parentElement.children) as any,
              columnElement
            );
          this.columnWidthsUpdated.emit(this.columnDescriptors);
        });
    }
  }

  private recalculateColumnDescriptors(movement: number,
                                       columnElements: HTMLElement[],
                                       columnElement: HTMLElement): ListColumnDescriptor[] {
    const columnIndex = columnElements.findIndex(isEqualTo(columnElement));
    return columnElements.map((element: HTMLElement, index: number) => {
      return this.calculateColumnWithFromChange(index, columnIndex, movement, element);
    });
  }

  private calculateColumnWithFromChange(index: number, columnIndex: number, movement: number, element: HTMLElement): ListColumnDescriptor {
    if (index === columnIndex) {
      return {...this.columns[index], width: `${element.clientWidth - movement}fr`};
    } else if (index === columnIndex - 1) {
      return {...this.columns[index], width: `${element.clientWidth + movement}fr`};
    } else {
      return {...this.columns[index], width: `${element.clientWidth}fr`};
    }
  }
}
