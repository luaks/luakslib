import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListResizeableHeaderComponent } from './list-resizeable-header.component';

describe('ListResizeableHeaderComponent', () => {
  let component: ListResizeableHeaderComponent;
  let fixture: ComponentFixture<ListResizeableHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListResizeableHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListResizeableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
