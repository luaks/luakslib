import {Component} from '@angular/core';
import {ListColumnDescriptor} from './list-header/list-column.descriptor';
import {range} from 'lodash';

@Component({
  selector: 'luaks-list-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  data = range(10).map(x => ({a: x, b: 'foo', c: 'blah', d: new Date(), iconName: 'face'}));

  columns: ListColumnDescriptor[] = [
    {
      title: 'Title A',
      width: '3fr'
    },
    {
      title: 'Title Bar',
      width: '5fr'
    },
    {
      title: 'Blah',
      width: '5fr'
    },
    {
      title: 'A date',
      width: '5fr'
    },
    {
      title: 'An icon',
      width: '5fr'
    }
  ];
}
