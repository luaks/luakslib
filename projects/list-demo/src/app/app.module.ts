import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ListComponent} from './list/list.component';
import {ListHeaderComponent} from './list-header/list-header.component';
import {ListResizeableHeaderComponent} from './list-resizeable-header/list-resizeable-header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {ListSlideInRowComponent} from './list-slide-in-row/list-slide-in-row.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ListHeaderComponent,
    ListResizeableHeaderComponent,
    ListSlideInRowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    ScrollingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
