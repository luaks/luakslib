import {Component} from '@angular/core';

@Component({
  selector: 'luaks-list-slide-in-row',
  templateUrl: './list-slide-in-row.component.html',
  styleUrls: [
    '../list-header/list-header.component.scss',
    './list-slide-in-row.component.scss'
  ]
})
export class ListSlideInRowComponent {
  slideInVisible: boolean = false;
}
