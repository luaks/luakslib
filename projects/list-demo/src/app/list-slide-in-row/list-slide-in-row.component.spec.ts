import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSlideInRowComponent } from './list-slide-in-row.component';

describe('ListSlideInRowComponent', () => {
  let component: ListSlideInRowComponent;
  let fixture: ComponentFixture<ListSlideInRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSlideInRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSlideInRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
