import {ChangeDetectionStrategy, Component, ContentChildren, ElementRef, Input, QueryList, TemplateRef} from '@angular/core';
import {ListColumnDescriptor} from './list-column.descriptor';

@Component({
  selector: 'luaks-list-header',
  templateUrl: './list-header.component.html',
  styleUrls: ['./list-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListHeaderComponent {
  @Input() columns: ListColumnDescriptor[];
  @Input() columnTemplate: TemplateRef<any>;

  @ContentChildren('.list-header__column', {read: ElementRef}) columnElements: QueryList<ElementRef>;

  trackByIndex(index: number): number {
    return index;
  }
}
