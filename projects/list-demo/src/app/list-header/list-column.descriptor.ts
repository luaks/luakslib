import {ListColumnWidthDescriptor} from '../list/listColumnWidthDescriptor';

export interface ListColumnDescriptor extends ListColumnWidthDescriptor {
  title: string;
}
